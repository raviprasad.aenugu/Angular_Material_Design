import { NgModule } from "@angular/core";

import { Routes, RouterModule} from "@angular/router";

import { WelcomeComponent } from "./welcome/welcome.component";
import { TrainingComponent } from "./training/training.component";
import { SignupComponent } from "./auth/signup/signup.component";
import { SigninComponent } from "./auth/signin/signin.component";





const   routes:Routes   = [
    {path :'', component :SignupComponent},
    {path :'welcome', component :WelcomeComponent},
    {path :'training', component :TrainingComponent},
    {path :'signup', component :SignupComponent},
    {path :'signin', component :SigninComponent}
];
@NgModule({
    declarations: [
    ],
    imports: [ RouterModule.forRoot(routes)  ],
    exports: [RouterModule],
    providers: [],
    bootstrap: []
  })
  

export class AppRoutingModule{


}