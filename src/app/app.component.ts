import { Component, ViewChild, OnInit } from '@angular/core';

import { AngularFirestore } from 'angularfire2/firestore';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent   implements OnInit{



  constructor( private db: AngularFirestore) {


  }
    
  @ViewChild('LocSideNav')
  title = 'app';


    ngOnInit(){
        
      this.db.collection('SampleData').valueChanges().subscribe(res =>{

        console.log(res);  
      })
    }

} 




